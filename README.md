

A flask api that handles all the following

1. Key listing and management
2. To all the keys, set custom pillars 
3. For all minions , list all the basic in-moment system metrics like CPU, memory, disk etc.


Excessive features are coming in as  plugins.

## Planned plugins :

1. User management with auto handle ssh key management from github, gitlab or from ftp server etc.
2. App installation across servers or selective servers and relative status
3. Alerting system , for the in-moment metrics that are collected and alert via email
4. Front end UI to manage all the apps.
5. Metric collection module and alerting based on metic history
    
    Each of the plugin would have its own repository. Refer to each of the repository for its own installation instructions. 

#### PS : Flask is still not decided. The web api framework is still a call that will be taken soon.